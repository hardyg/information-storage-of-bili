# 小志小视界-Bilibili资料存放

#### 介绍
- 本仓库主要存放用于哔哩哔哩账号：**小志小视界**所发布的各种视频的相关资料
- 小志小视界账号主要发布关于Stata基础学习和科研学习有关的视频
- 如何从码云平台下载资料：[科研基础-码云平台文件下载：以profile do文件为例](https://www.bilibili.com/video/BV1iy4y1z7Pv/)

#### Bilibili（哔哩哔哩）账号：
- 可直接在官网或App搜索**小志小视界**即可
- 也可以点击[小志小视界](https://space.bilibili.com/482149859)

### 代表作品(点击标题即可)
- [连享会直播Stata小白的取经之路-Stata学习分享从入门到进阶](https://www.bilibili.com/video/BV1ED4y1R7et)
- [综合练习：实证研究的Stata实现，毕业论文不用愁！](https://www.bilibili.com/video/BV1AD4y1d7s6)
- [Stata进阶：高效运用搜索解决计量和软件操作难题？](https://www.bilibili.com/video/BV1K54y1k7ej)
- [Stata设置-如何正确设置连玉君老师的profile.do文件](https://www.bilibili.com/video/BV1yT4y1E7KQ/)
- [Stata设置-如何快速安装3000+外部命令（连玉君老师PLUS文件）](https://www.bilibili.com/video/BV1Si4y1s7K6/)

--- 
### Stata小白的实证论文取经之路（基础篇）

#### 只学三个
- [综合练习：实证研究的Stata实现，毕业论文不用愁！](https://www.bilibili.com/video/BV1AD4y1d7s6/)
- [Stata进阶：高效运用搜索解决计量和软件操作难题？](https://www.bilibili.com/video/BV1K54y1k7ej/)
- [连享会直播-Stata学习分享从入门到进阶](https://www.bilibili.com/video/BV1ED4y1R7et/)

#### Stata软件
- [Stata基础-Stata界面简介&可重复性研究](https://www.bilibili.com/video/BV1hz411e7RL/)
- [Stata设置-中英文转换与界面背景调试](https://www.bilibili.com/video/BV1mD4y1Q7qQ/)
- [Stata基础-如何快速搞懂Stata命令和语法](https://www.bilibili.com/video/BV145411W7kM/)
- [Stata基础：do文档中代码注释的三种姿势](https://www.bilibili.com/video/BV1Qk4y1m7wM/)
- [Stata基础：代数，逻辑和关系运算符](https://www.bilibili.com/video/BV1Kv411r7MH/)
- [Stata基础-外部命令的安装与使用](https://www.bilibili.com/video/BV1Tr4y1A71j/)
- [Stata基础-系统变量_n和_N](https://www.bilibili.com/video/BV1KV411Y72c/)
- [Stata基础：解读Stata内存中的返回值/留存值](https://www.bilibili.com/video/BV1gV41127TM/)
- [Stata设置-如何正确设置连玉君老师的profile.do文件](https://www.bilibili.com/video/BV1yT4y1E7KQ/)
- [科研基础-码云平台文件下载：以profile do文件为例](https://www.bilibili.com/video/BV1iy4y1z7Pv/)
- [Stata设置-如何快速安装3000+外部命令（连玉君老师PLUS文件）](https://www.bilibili.com/video/BV1Si4y1s7K6/)
- [资源分享：Stata快速入门小册子](https://www.bilibili.com/video/BV1yz4y1C7Hp/)

#### 数据下载：
-  [数据专题：国泰安（CSMAR）数据库使用简介与数据下载演示](https://www.bilibili.com/video/BV1bK411J7fV/)
-  [Stata基础: 如何从CSMAR下载数据中快速提取年份数据](https://www.bilibili.com/video/BV1u64y1c7cX/)

#### 数据导入
- [如何将数据从Excel中导入到Stata？](https://www.bilibili.com/video/BV18A411i77s/)
- [Stata进阶：如何将市场化指数正确导入Stata及转换为面板](https://www.bilibili.com/video/BV1mi4y1g7p3/)
- [Stata进阶：如何批量导入数据（以Excel数据为例）](https://www.bilibili.com/video/BV1Ha4y1H7XM/)
- [Stata基础：如何批量导入Excel的多张表单(Sheet)](https://www.bilibili.com/video/BV1st4y1k7VW/)

#### 简单命令
- [Stata基础：如何产生一个新变量-gen & egen](https://www.bilibili.com/video/BV1V64y1F78k/)
- [Stata基础：虚拟变量的生成](https://www.bilibili.com/video/BV1Eh411R7f6/)
- [Stata基础：i.variable与虚拟变量陷阱](https://www.bilibili.com/video/BV1kt4y1i7G6/)
- [Stata基础-Split分割函数:以日期型数据和高管学术经历为例](https://www.bilibili.com/video/BV1Wh411f7mb/)
- [Stata基础：字符串处理函数小结](https://www.bilibili.com/video/BV1Tp4y1Y7tr/)
- [Stata基础-分组执行命令(bysort)](https://www.bilibili.com/video/BV1iy4y1h7ty/)
- [Stata基础-group函数:分组连续编号](https://www.bilibili.com/video/BV1ny4y1H7Fn/)
- [Stata基础-组内交叉合并(joinby)](https://www.bilibili.com/video/BV1XU4y147Q8/)
- [Stata基础：暂元-暂时性的存储单元(local&global)](https://www.bilibili.com/video/BV1Si4y1g7uX/)
- [Stata基础：循环-完成重复性任务（forvalues&foreach）](https://www.bilibili.com/video/BV1aC4y1t7Jg/)

#### 数据处理：
- [Stata基础：数据窗口"红黑蓝"，何种数据类型？](https://www.bilibili.com/video/BV1rt4y1U7Zg/)
- [Stata基础：如何快速处理数据中的日期类型变量](https://www.bilibili.com/video/BV1Gv411i7gQ/)
- [Stata基础-如何快速删除样本中的重复值？](https://www.bilibili.com/video/BV1Kp4y1v7XK/)
- [Stata基础-"重复值"处理，不要误伤"自己人"](https://www.bilibili.com/video/BV16541187b7/)
- [Stata基础：如何处理数据中的极端值或异常值？](https://www.bilibili.com/video/BV1yt4y1U73m/)
- [Stata基础：如何处理样本中的缺失值](https://www.bilibili.com/video/BV1yT4y1L7UD/)
- [Stata基础：如何进行数据横向合并与纵向追加](https://www.bilibili.com/video/BV1qT4y1w7sX/)

#### 变量构建
- [Stata实操：高管个人经历的关键变量构建](https://www.bilibili.com/video/BV1Ky4y1q7aU/)
- 需要更多的视频

#### 实证部分
- [Stata基础：描述性统计结果输出及解读运用](https://www.bilibili.com/video/BV1L54y1m7pW/)
- [Stata基础：相关性分析结果输出与解读运用](https://www.bilibili.com/video/BV1qA411E7ZN/)
- [Stata基础：单变量差异检验结果输出与解读运用](https://www.bilibili.com/video/BV1L5411877n/)
- [Stata基础：如何选择合适的回归命令？](https://www.bilibili.com/video/BV1Uz4y1d7Ux/)
- [Stata基础：如何在回归中控制行业效应](https://www.bilibili.com/video/BV1DV411m7um/)
- [Stata基础：如何快速输出Stata回归结果？](https://www.bilibili.com/video/BV1i5411j7Ef/)
- [Stata基础：截面数据Stata回归结果解读](https://www.bilibili.com/video/BV1YT4y1c7eM/)
- [Stata基础：如何对截面数据分组](https://www.bilibili.com/video/BV1wV411Y7NW/)
- [Stata基础：交互效应和分组回归（程序实现）](https://www.bilibili.com/video/BV1pK411N7bt/)
- [Stata基础：稳健性检验！！！](https://www.bilibili.com/video/BV14a411c7mR/)

#### 面板数据
- [Stata基础：截面数据&面板数据（概念篇）](https://www.bilibili.com/video/BV1za411A7kb/)
- [Stata基础：截面数据&面板数据（方法篇）](https://www.bilibili.com/video/BV1Wi4y177sg/)
- [Stata基础：如何正确设定面板数据](https://www.bilibili.com/video/BV1LT4y1F7yC/)
- [Stata基础-面板数据模式解析(xtpattern)](https://www.bilibili.com/video/BV1Tv41147H4/)
- [Stata基础：固定效应模型](https://www.bilibili.com/video/BV1zZ4y1g79q/)
- [Stata基础：聚类稳健标准误(Robust)](https://www.bilibili.com/video/BV1Yz4y1r7Yd/)

#### 进阶学习
- [如何快速获取经济金融权威期刊论文的数据和程序](https://www.bilibili.com/video/BV1c5411e7aZ/)
- [学习资源分享-计量经济大佬喊你快来免费学习！！！](https://www.bilibili.com/video/BV1P54y1q7XA/)

#### 其他科研视频
- [科研基础：思考研究问题的框架-Libby's box](https://www.bilibili.com/video/BV1Uv41167ff/)
- [科研基础：不容错过的经济金融会计期刊](https://www.bilibili.com/video/BV1ii4y177Hg/)
- [科研小技巧：如何从知网高效检索研究领域的重要文献](https://www.bilibili.com/video/BV18Z4y1H7Mi/)
- [科研小技巧：如何从Web of Science聚合检索研究话题的权威文献](https://www.bilibili.com/video/BV1Ak4y117jx/)
- [资源推荐：《因果推断实用计量方法》](https://www.bilibili.com/video/BV1uy4y187Zo/)